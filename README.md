Front End Guide At Intercom
==

### Table of Contents

- [Angular Framework](#angular-framework)
- [State Management](#state-management---akita)
- [CSS - SCSS](#css-scss)
- [HTML](#html)

## Angular Framework

Angular Style Guide:

- **Separate filenames with dots and dashes** - Do use conventional type names including .service, .component, .pipe, .module, and .directive.

- **Symbols and file names** - 
Do use upper camel case for class names.
Do match the name of the symbol to the name of the file.
Do give the filename the conventional suffix (such as .component.ts, .directive.ts, .module.ts, .pipe.ts, or .service.ts) for a file of that type.

- **Component selectors** - Do use dashed-case or kebab-case for naming the element selectors of components.
- **Interfaces** - 
Do name an interface using upper camel case.
Consider naming an interface without an I prefix.
- **Overall structural guidelines** -
```
|-- app
     |-- modules
       |-- home
           |-- [+] components
           |-- home-routing.module.ts
           |-- home.module.ts
     |-- core
       |-- [+] authentication
       |-- [+] guards
       |-- [+] http
       |-- [+] interceptors
       |-- [+] services
       |-- core.module.ts
     |
     |-- shared
          |-- [+] components
          |-- [+] directives
          |-- [+] pipes
          |-- [+] models
          |-- [+] enums
          |-- [+] validators
     |
     |-- [+] configs
|-- assets
    |-- scss
          |-- [+] partials
          |-- _base.scss
          |-- variables.scss
          |-- styles.scss
    |-- i18n
          |-- lang-a.json
          |-- lang-b.json
    |-- images
          |-- img.png
    |-- icons
          |-- icon-a.svg
```
- **TrackBy** - When using ngFor to loop over an array in templates, use it with a trackByfunction which will return an unique identifier for each item.
When an array changes, Angular re-renders the whole DOM tree. But if you use trackBy, Angular will know which element has changed and will only make DOM changes for that particular element.

```
// in the template

<li *ngFor="let item of items; trackBy: trackByFn">{{ item }}</li>

// in the component

trackByFn(index, item) {    
   return item.id; // unique id corresponding to the item
}
```

- **Clean up subscriptions** - When subscribing to observables, always make sure you unsubscribe from them appropriately by using operators like take, takeUntil, etc.

```
private _destroyed$ = new Subject();

public ngOnInit (): void {
    iAmAnObservable
    .pipe(
       map(value => value.item)
      // We want to listen to iAmAnObservable until the component is destroyed,
       takeUntil(this._destroyed$)
     )
    .subscribe(item => this.textToDisplay = item);
}

public ngOnDestroy (): void {
    this._destroyed$.next();
    this._destroyed$.complete();
}
```

- **Clean up imports with path aliases** -
We can clean up these imports considerably by using aliases to reference our files, which looks something like this:
```
import 'reusableComponent' from '@app/shared/components/reusable.component.ts';
```

To be able to do this, we need to add a baseUrl and the desired paths inside our tsconfig.json file:
```
{
  "compilerOptions": {
    ...
    "baseUrl": "src",
    "paths": {
      "@app:": ["@app/*"]
    }
  }
}
```

- **Codelyzer** - Use [codelyzer](https://www.npmjs.com/package/codelyzer) to follow this guide.
Consider adjusting the rules in codelyzer to suit your needs

- **Use TSLint** - This is a static code analysis tool we use in software development for checking if TypeScript code complies with the coding rules

- **Prettier with TSLint** - You can combine TSLint with Prettier. Prettier is an amazing tool that enforces a consistent style by parsing your code and re-printing it, with its own rules in place. Having Prettier setup with TSLint gives you a strong foundation for your applications, as you no longer need to maintain your code-style manually. Prettier takes care of code formatting and TSLint handles the rest.

- **Lint with Husky** - Even with these rules in place, it can be hard to maintain them. You can easily forget to run these commands before pushing your code to production, which leads to a broken result. One way to work around this problem is by using husky. Husky allows us to run custom scripts before the staged files are committed — keeping our production code clean and organized.

## State Management - Akita
We Used [Akita](https://datorama.github.io/akita/) to manage all states in your prject.

## CSS - SCSS
We follow the [BEM](http://getbem.com/introduction/) naming convention.

## HTML
We follow the [Semantic Elements](https://www.w3schools.com/html/html5_semantic_elements.asp)
